#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)
    
    def test_read_2(self):
        s = "100 200\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  100)
        self.assertEqual(j, 200)

    def test_read_3(self):
        s = "201 210\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  201)
        self.assertEqual(j, 210)



    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 19)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 124)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 88)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 173)

    def test_eval_5(self):
        v = collatz_eval(11, 30)
        self.assertEqual(v, 111)
    
    def test_eval_6(self):
        v = collatz_eval(9, 99)
        self.assertEqual(v, 118)

    def test_eval_7(self):
        v = collatz_eval(2, 3)
        self.assertEqual(v, 7)
    
    def test_eval_8(self):
        v = collatz_eval(10, 1)
        self.assertEqual(v, 19)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 100, 200, 124)
        self.assertEqual(w.getvalue(), "100 200 124\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 11, 30, 111)
        self.assertEqual(w.getvalue(), "11 30 111\n")


    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 19\n")
    
    def test_solve_2(self):
        r = StringIO("100 200\n201 210\n900 1000\n11 30\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "100 200 124\n201 210 88\n900 1000 173\n11 30 111\n")

    def test_solve_3(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n11 30\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 19\n100 200 124\n201 210 88\n900 1000 173\n11 30 111\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
